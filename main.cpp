#include <TGUI/TGUI.hpp>
#include <Table.hpp>
#include <SFML/Graphics.hpp>

int main() {
    sf::RenderWindow window({800,600}, "tgui::Table");

    tgui::Gui gui(window);
    gui.setFont("DejaVuSans.ttf");

    tgui::Theme::Ptr theme = std::make_shared<tgui::Theme>("Black.txt");

    tgui::Button::Ptr buttonRow = theme->load("button");
    buttonRow->setText("Connect");
    buttonRow->setSize({100,30});

    auto tableRow = std::make_shared<tgui::TableRow>();
    tableRow->addItem("Alex");
    tableRow->addItem("Mircea");
    tableRow->add(buttonRow, true);

    auto table = std::make_shared<tgui::Table>();
    table->setFont(gui.getFont());
    table->setSize({780, 580});
    table->setPosition({10,10});
    table->setHeaderColumns({"Name", "IP", ""});
    table->setBackgroundColor({203,201,207});
    table->setFixedColumnWidth(0, 400);
    table->add(tableRow);
    table->addRow({"Ghita", "Ion", "Vlad"});
    table->addRow({"Vasile", "Matei", "Nicolae"});
    table->setStripesRowsColor({246,246,246}, {233,233,233});
    gui.add(table);

    while (window.isOpen()) {
        sf::Event event;
    	while(window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            gui.handleEvent(event);
        }

        window.clear({210, 45, 20});
        gui.draw();
        window.display();
    }

    return 0;
}
