#include <Table.hpp>

namespace tgui {
    Table::Table() {
        m_callback.widgetType = "Table";

        m_tableBorder.setFillColor(sf::Color::Transparent);
        m_tableBorder.setOutlineColor(sf::Color::Green);
        m_tableBorder.setOutlineThickness(1);

        m_headerSeparator.setFillColor(sf::Color::Green);
        //m_headerSeparator.setOutlineThickness(1);
    }

    void Table::setFont(const Font& font) {
        BoxLayout::setFont(font);
        calculateLabelHeight();

        if (m_header) m_header->setFont(font);
    }

    void Table::setCharacterSize(unsigned int size) {
        m_characterSize = size;
        if (getFont() != nullptr) calculateLabelHeight();
    }

    void Table::setSize(const Layout2d& size) {
        BoxLayout::setSize(size);
        m_headerSeparator.setSize(sf::Vector2f(getSize().x, 1.f));
        m_tableBorder.setSize(getSize());

        updateColumnsDelimitatorsSize();

        for (std::size_t i=0; i<m_layoutWidgets.size(); ++i) {
            m_layoutWidgets[i]->setSize(getSize().x, m_layoutWidgets[i]->getSize().y);
        }
    }

    bool Table::insert(std::size_t index, const tgui::Widget::Ptr& widget, const sf::String& widgetName) {
        for (std::size_t i=0; i<m_columnsFixedWidth.size(); ++i) {
            if (m_columnsFixedWidth[i] != 0) {
                std::dynamic_pointer_cast<HorizontalLayout>(widget)->setFixedSize(i, m_columnsFixedWidth[i]);
            } else {
                std::dynamic_pointer_cast<HorizontalLayout>(widget)->setRatio(i, m_header->getRatio(i));
            }
        }

        if (m_rowsEvenColor == sf::Color::Transparent) std::dynamic_pointer_cast<TableRow>(widget)->setNormalBackgroundColor(m_rowsOddColor);
        else if (index % 2 == 0) std::dynamic_pointer_cast<TableRow>(widget)->setNormalBackgroundColor(m_rowsEvenColor);
        else std::dynamic_pointer_cast<TableRow>(widget)->setNormalBackgroundColor(m_rowsOddColor);

        float customHeight = std::dynamic_pointer_cast<TableRow>(widget)->getCustomHeight();
        widget->setSize(getSize().x, (customHeight > m_rowHeight) ? customHeight : m_rowHeight);

        return BoxLayout::insert(index, widget, widgetName);
    }

    void Table::add(const tgui::Widget::Ptr& widget, const sf::String& widgetName) {
        insert(m_layoutWidgets.size(), widget, widgetName);
    }

    void Table::addRow(const std::vector<std::string>& columns) {
        auto row = std::make_shared<TableRow>();
        for (const std::string& column : columns) {
            row->addItem(column, m_normalTextColor);
        }
        insert(m_layoutWidgets.size(), row, "");
    }

    void Table::setHeader(const tgui::TableRow::Ptr& row) {
        m_header = row;
        m_header->setFont(getFont());

        m_columnsDelimitators.resize(row->getWidgets().size()-1);

        m_columnsFixedWidth.resize(m_columnsDelimitators.size()+1);

        updateWidgetPositions();
    }

    void Table::setHeaderColumns(const std::vector<std::string>& columns) {
        m_header = std::make_shared<tgui::TableRow>();

        for (const std::string& column : columns) {
            m_header->addItem(column, sf::Color::White);
        }

        m_header->setNormalBackgroundColor({234,97,83});
        setHeader(m_header);
    }

    void Table::setFixedColumnWidth(std::size_t column, float size) {
        // What if the user use setFixedColumnWidth and setRatioColumnWidth on same column?
        m_columnsFixedWidth[column] = size;

        m_header->setFixedSize(column, size);
        for (unsigned int i=0; i<m_layoutWidgets.size(); ++i) {
            std::dynamic_pointer_cast<HorizontalLayout>(m_layoutWidgets[i])->setFixedSize(column, size);
        }

        updateColumnsDelimitatorsPosition();
    }

    void Table::setColumnRatio(std::size_t column, std::size_t ratio) {
        // What if the user use setFixedColumnWidth and setRatioColumnWidth on same column?
        m_header->setRatio(column, ratio);
        for (unsigned int i=0; i<m_layoutWidgets.size(); ++i) {
            std::dynamic_pointer_cast<HorizontalLayout>(m_layoutWidgets[i])->setRatio(column, ratio);
        }

        updateColumnsDelimitatorsPosition();
    }

    void Table::setRowsColor(const sf::Color& color) {
        m_rowsOddColor = color;
        m_rowsEvenColor = sf::Color::Transparent;
    }

    void Table::setStripesRowsColor(const sf::Color& evenColor, const sf::Color& oddColor) {
        m_rowsOddColor = oddColor;
        m_rowsEvenColor = evenColor;

        for (std::size_t i=0; i<m_layoutWidgets.size(); ++i) {
            if (i % 2 == 0 ) std::dynamic_pointer_cast<TableRow>(m_layoutWidgets[i])->setNormalBackgroundColor(evenColor);
            else std::dynamic_pointer_cast<TableRow>(m_layoutWidgets[i])->setNormalBackgroundColor(oddColor);

            std::dynamic_pointer_cast<TableRow>(m_layoutWidgets[i])->setHoverBackgroundColor({255,255,0});
        }
    }

    void Table::setTextColor(const sf::Color& color) {
        m_normalTextColor = color;
    }

    void Table::updateWidgetPositions() {
        if (m_header != nullptr) {
            m_header->setPosition(0, 0);
            m_header->setSize(getSize().x, m_rowHeight);
            updateColumnsDelimitatorsPosition();
            m_headerSeparator.setPosition(0, m_rowHeight);
        }

        if (m_layoutWidgets.empty()) return;
        m_layoutWidgets[0]->setPosition(0, m_header->getSize().y + m_header->getPosition().y + 1.f);

        for (unsigned int i=1; i<m_layoutWidgets.size(); ++i) {
            m_layoutWidgets[i]->setPosition(0, m_layoutWidgets[i-1]->getSize().y + m_layoutWidgets[i-1]->getPosition().y + 1.f);
        }
    }

    void Table::calculateLabelHeight() {
        auto label = std::make_shared<Label>();
        label->setFont(getFont());
        label->setTextSize(m_characterSize);

        m_rowHeight = label->getSize().y;
    }

    void Table::updateColumnsDelimitatorsPosition() {
        auto headerWidgets = m_header->getWidgets();
        for (unsigned int i=1; i<headerWidgets.size(); ++i) {
            m_columnsDelimitators[i-1].setPosition(headerWidgets[i]->getPosition().x, 0);
        }
    }

    void Table::updateColumnsDelimitatorsSize() {
        for (unsigned int i=0; i<m_columnsDelimitators.size(); ++i) {
            m_columnsDelimitators[i].setFillColor(sf::Color::Black);
            m_columnsDelimitators[i].setSize({1.f, getSize().y});
        }
    }

    void Table::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // Set the position
        states.transform.translate(getPosition());

        // Draw the background
        if (m_backgroundColor != sf::Color::Transparent)
        {
            sf::RectangleShape background(getSize());
            background.setFillColor(m_backgroundColor);
            target.draw(background, states);
        }

        target.draw(m_tableBorder, states);

        // Draw the widgets
        target.draw(*m_header, states);
        drawWidgetContainer(&target, states);

        //target.draw(m_headerSeparator, states);

        for (unsigned int i=0; i<m_columnsDelimitators.size(); ++i) {
            target.draw(m_columnsDelimitators[i], states);
        }
    }
}
