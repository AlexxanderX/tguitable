#ifndef XAL_TGUI_TABLEROW
#define XAL_TGUI_TABLEROW

#include <TGUI/HorizontalLayout.hpp>
#include <TGUI/Widgets/Label.hpp>
#include <TableItem.hpp>

namespace tgui {
    class TableRow: public HorizontalLayout {
    public:
        typedef std::shared_ptr<TableRow> Ptr; ///< Shared widget pointer
        typedef std::shared_ptr<const TableRow> ConstPtr; ///< Shared constant widget pointer

        TableRow();

        // Maybe return bool and to check if the row has the table columns number
        virtual void setItem(unsigned int column, const std::string& name, TableItem::HorizontalAlign align = TableItem::None);
        virtual void setItem(unsigned int column, const std::string& name, const sf::Color& color, TableItem::HorizontalAlign align = TableItem::None);

        virtual void addItem(const std::string& name);
        virtual void addItem(const std::string& name, const sf::Color& color);
        virtual void addItems(const std::vector<std::string>& columns, const sf::Color& color, TableItem::HorizontalAlign align = TableItem::None);

        virtual bool insert(std::size_t index, const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, const sf::String& widgetName = "");
        virtual void add(const tgui::Widget::Ptr& widget, const sf::String& widgetName = "") override;
        virtual void add(const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, const sf::String& widgetName = "");
        virtual void add(const tgui::Widget::Ptr& widget, bool fixedHeight, TableItem::HorizontalAlign align = TableItem::None, const sf::String& widgetName = "");

        virtual void setHoverBackgroundColor(const sf::Color& color);
        virtual void setNormalBackgroundColor(const sf::Color& color);

        // virtual void setHoverTextColor(const sf::Color& color);
        virtual void setNormalTextColor(const sf::Color& color);

        virtual void setItemsHorizontalAlign(TableItem::HorizontalAlign align);

        virtual bool mouseOnWidget(float x, float y);

        virtual float getCustomHeight() { return m_customHeight; } // internal
    protected:
        virtual TableItem::Ptr createItem(const std::string& name, TableItem::HorizontalAlign align, const sf::Color& color = sf::Color::Black);
        virtual TableItem::Ptr createItem(const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, bool fixedHeight = false);

        virtual void updateTextColor();

    protected:
        sf::Color m_normalBackgroundColor;
        sf::Color m_hoverBackgroundColor = sf::Color::Transparent;

        sf::Color m_normalTextColor;

        TableItem::HorizontalAlign m_align = TableItem::Left;

        float m_customHeight = 0.f;
    };
}

#endif
