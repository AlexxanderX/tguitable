#ifndef XAL_TGUI_TABLEITEM
#define XAL_TGUI_TABLEITEM

#include <TGUI/Widget.hpp>
#include <TGUI/Widgets/Label.hpp>

namespace tgui {
    class TableItem: public Widget {
    public:
        enum HorizontalAlign {
            Left,
            Center,
            Right,
            None /// internal
        };

    public:
        typedef std::shared_ptr<TableItem> Ptr; ///< Shared widget pointer
        typedef std::shared_ptr<const TableItem> ConstPtr; ///< Shared constant widget pointer

        TableItem();

        virtual Widget::Ptr clone() const override { return std::make_shared<TableItem>(*this); }

        void setItem(const Widget::Ptr& widgetPtr, HorizontalAlign align = Left);
        Widget::Ptr getItem() { return m_widget; }

        void setHorizontalAlign(HorizontalAlign align);

        void setFont(const Font& font);

        virtual void setSize(const Layout2d& size) override;
        using Transformable::setSize;

        virtual sf::Vector2f getFullSize() const override { return getSize(); }

        virtual bool mouseOnWidget(float x, float y) override;

    protected:
        //virtual void updateWidgetPositions();
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

        void update();

    protected:
        Widget::Ptr m_widget;

        HorizontalAlign m_align;
    };
}

#endif
