#include <TableRow.hpp>

namespace tgui {
    TableRow::TableRow() {
        m_callback.widgetType = "TableRow";
    }

    void TableRow::setItem(unsigned int column, const std::string& name, TableItem::HorizontalAlign align) {
        BoxLayout::insert(column, createItem(name, align, m_normalTextColor));
    }

    void TableRow::setItem(unsigned int column, const std::string& name, const sf::Color& color, TableItem::HorizontalAlign align) {
        BoxLayout::insert(column, createItem(name, align, color));
    }

    void TableRow::TableRow::addItem(const std::string& name) {
        BoxLayout::add(createItem(name, m_align, m_normalTextColor));
    }

    void TableRow::addItem(const std::string& name, const sf::Color& color) {
        BoxLayout::add(createItem(name, m_align, color));
    }

    void TableRow::addItems(const std::vector<std::string>& columns, const sf::Color& color, TableItem::HorizontalAlign align) {
        for (std::string column : columns) {
            BoxLayout::add(createItem(column, align, color));
        }
    }

    bool TableRow::insert(std::size_t index, const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, const sf::String& widgetName) {
        return HorizontalLayout::insert(index, createItem(widget, align), widgetName);
    }

    void TableRow::add(const tgui::Widget::Ptr& widget, const sf::String& widgetName) {
        HorizontalLayout::insert(m_layoutWidgets.size(), createItem(widget, m_align), widgetName);
    }

    void TableRow::add(const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, const sf::String& widgetName) {
        HorizontalLayout::insert(m_layoutWidgets.size(), createItem(widget, align), widgetName);
    }

    void TableRow::add(const tgui::Widget::Ptr& widget, bool fixedHeight, TableItem::HorizontalAlign align, const sf::String& widgetName) {
        HorizontalLayout::insert(m_layoutWidgets.size(), createItem(widget, (align == TableItem::None) ? m_align : align, fixedHeight), widgetName);
    }

    void TableRow::setHoverBackgroundColor(const sf::Color& color) {
        m_hoverBackgroundColor = color;
    }

    void TableRow::setNormalBackgroundColor(const sf::Color& color) {
        m_normalBackgroundColor = color;
        setBackgroundColor(color);
    }

    void TableRow::setNormalTextColor(const sf::Color& color) {
        m_normalTextColor = color;
    }

    void TableRow::setItemsHorizontalAlign(TableItem::HorizontalAlign align) {
        m_align = align;

        for (std::size_t i=0; i<m_layoutWidgets.size(); ++i) {
            std::dynamic_pointer_cast<TableItem>(m_layoutWidgets[i])->setHorizontalAlign(align);
        }
    }

    bool TableRow::mouseOnWidget(float x, float y) {
        bool isOnWidget = BoxLayout::mouseOnWidget(x,y);

        if (isOnWidget && m_hoverBackgroundColor != sf::Color::Transparent) {
            setBackgroundColor(m_hoverBackgroundColor);
        } else {
            setBackgroundColor(m_normalBackgroundColor);
        }

        return isOnWidget;
    }

    TableItem::Ptr TableRow::createItem(const std::string& name, TableItem::HorizontalAlign align, const sf::Color& color) {
        auto label = std::make_shared<tgui::Label>();
        label->setText(name);
        label->setTextColor(color);

        auto layout = std::make_shared<tgui::TableItem>();
        layout->setItem(label, (align == TableItem::None) ? m_align : align);

        return layout;
    }

    TableItem::Ptr TableRow::createItem(const tgui::Widget::Ptr& widget, TableItem::HorizontalAlign align, bool fixedHeight) {
        if (fixedHeight && widget->getSize().y > m_customHeight) {
            m_customHeight = widget->getSize().y;
        }

        auto layout = std::make_shared<tgui::TableItem>();
        layout->setItem(widget, align);

        return layout;
    }

    void TableRow::updateTextColor() {
        for (std::size_t i=0; i<m_layoutWidgets.size(); ++i) {
            //std::dynamic_pointer_cast<TableItem>(m_layoutWidgets[i])->std::dynamic_pointer_cast<TableItem>(m_layoutWidgets[i])getWidget()
        }
    }
}
