#include <TableItem.hpp>

namespace tgui {
    TableItem::TableItem() {
        m_callback.widgetType = "TableItem";
    }

    void TableItem::setItem(const Widget::Ptr& widgetPtr, HorizontalAlign align) {
        m_widget = widgetPtr;
        m_align = align;
        update();
    }

    void TableItem::setHorizontalAlign(HorizontalAlign align) {
        m_align = align;
        update();
    }

    void TableItem::setFont(const Font& font) {
        m_widget->setFont(font);
        update();
    }

    void TableItem::setSize(const Layout2d& size) {
        Widget::setSize(size);
        update();
    }

    bool TableItem::mouseOnWidget(float x, float y) {
        return m_widget->mouseOnWidget(x-getPosition().x,y-getPosition().y);
    }

    void TableItem::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        if (m_widget != nullptr) {
            target.draw(*m_widget, states);
        }
    }

    void TableItem::update() {
        if (m_widget != nullptr) {
            if (m_align == Left) {
                m_widget->setPosition(getPosition().x + 5.f,
                                     getPosition().y + getSize().y/2 - m_widget->getSize().y/2);
            } else if (m_align == Center) {
                m_widget->setPosition(getPosition().x + getSize().x/2 - m_widget->getSize().x/2,
                                     getPosition().y + getSize().y/2 - m_widget->getSize().y/2);
            } else {
                m_widget->setPosition(getPosition().x + getSize().x - m_widget->getSize().x,
                                     getPosition().y + getSize().y/2 - m_widget->getSize().y/2);
            }

        }
    }
}
