#ifndef XAL_TGUI_TABLE
#define XAL_TGUI_TABLE

#include <TGUI/BoxLayout.hpp>
#include <TableRow.hpp>

namespace tgui {
    class Table: public BoxLayout {
    public:
        typedef std::shared_ptr<Table> Ptr; ///< Shared widget pointer
        typedef std::shared_ptr<const Table> ConstPtr; ///< Shared constant widget pointer

        Table();

        virtual void setFont(const Font& font) override;
        virtual void setCharacterSize(unsigned int size);

        virtual void setSize(const Layout2d& size) override;
        using Transformable::setSize;

        virtual bool insert(std::size_t index, const tgui::Widget::Ptr& widget, const sf::String& widgetName = "");
        virtual void add(const tgui::Widget::Ptr& widget, const sf::String& widgetName = "") override;
        virtual void addRow(const std::vector<std::string>& columns);

        virtual void setHeader(const tgui::TableRow::Ptr& row);
        virtual void setHeaderColumns(const std::vector<std::string>& columns);
        // getHeader()

        virtual void setFixedColumnWidth(std::size_t column, float size);
        virtual void setColumnRatio(std::size_t column, std::size_t ratio);

        // virtual void setFixedRowsHeight(float size);

        // void showTableBorders(bool show = true);
        // void showColumnsBorders(bool show = true);
        // void setTableBordersColor(const sf::Color& color);
        // void setColumnsBorder(const sf::Color& color);

        virtual void setRowsColor(const sf::Color& color);
        virtual void setStripesRowsColor(const sf::Color& evenColor, const sf::Color& oddColor);

        virtual void setTextColor(const sf::Color& color);
    protected:
        virtual void updateWidgetPositions();
        virtual void calculateLabelHeight();
        virtual void updateColumnsDelimitatorsPosition();
        virtual void updateColumnsDelimitatorsSize();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    protected:
        tgui::TableRow::Ptr m_header = nullptr;

        float m_rowHeight;

        std::vector<float> m_columnsFixedWidth;

        sf::RectangleShape m_tableBorder;
        sf::RectangleShape m_headerSeparator;
        std::vector<sf::RectangleShape> m_columnsDelimitators;
        sf::Color m_bordersColor;

        sf::Color m_rowsOddColor;
        sf::Color m_rowsEvenColor = sf::Color::Transparent;

        unsigned int m_characterSize = 18;

        sf::Color m_normalTextColor = sf::Color::Black;
    };
}

#endif
